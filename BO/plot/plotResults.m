function plotResults(fun_name)
%   PLOT 
%   simple regret : min_xt f(xt)-f(x*) where f(x*) is the global minimum
%   average cumultative regret : sum^T {f(xt)-f(x*)}/T
%

%% read data and set output path
result_folder=[getenv('HOME_DIR') 'results/'];
if strcmp(fun_name,'travel_time')
    ga=load([result_folder 'ga_result_travel_time']);
    random=load([result_folder 'random_result_travel_time']);
    bo=load([result_folder 'result_travel_time']);
    simple_regret=[bo.simple_regret',ga.simple_regret',random.simple_regret'];
    avr_cumulative_regret=[bo.avr_cumulative_regret',ga.avr_cumulative_regret',random.avr_cumulative_regret'];
elseif strcmp(fun_name,'monetary_penalty')
    ga=load([result_folder 'ga_result_monetary_penalty']);
    random=load([result_folder 'random_result_monetary_penalty']);
    bo=load([result_folder 'result_monetary_penalty']);
    simple_regret=[bo.simple_regret',ga.simple_regret',random.simple_regret'];
    avr_cumulative_regret=[bo.avr_cumulative_regret',ga.avr_cumulative_regret',random.avr_cumulative_regret'];
end

%% configure plot
leg={'original BO' 'genetic algorithm','random selection'}; % 5. legend
mark={'gv','y^','k+'}; 
color={'g','y','k'};
xtk=[0 10 20 30 40 50 60 70 80 90 100];
legend_interval=10;
n_iterations=length(simple_regret{1});
T=(0:n_iterations-1)';
mark_index=legend_interval:legend_interval:n_iterations;

%% plot simple regret
for method=1:3
    sr=reshape(cell2mat(simple_regret(:,method)),n_iterations,[]);
    mean_sr=mean(sr,2);
    mean_sr_samples=mean_sr(mark_index);
    plot(mark_index-1,mean_sr_samples,mark{method});
    hold on;
end
legend(leg{1:end});

for method=1:3
    sr=reshape(cell2mat(simple_regret(:,method)),n_iterations,[]);
    mean_sr=mean(sr,2);
    plot(T,mean_sr,color{method});
    mean_sr_samples=mean_sr(mark_index);
    std_sr=std(sr,0,2);
    std_sr_samples=std_sr(mark_index);
    errorbar(mark_index-1,mean_sr_samples,std_sr_samples/4,mark{method});
end


xlabel('Iterations');
if strcmp(fun_name,'monetary_penalty')
    ylabel('Simple Regret (SGD)');
elseif strcmp(fun_name,'travel_time')
    ylabel('Simple Regret (sec)');
end
xlim([0 105]);
set(gca,'XTick',xtk);
saveas(gcf,[result_folder fun_name '_simple_regret.eps'],'epsc2');
close all;

%% plot average of cumulative regret
for method=1:3
    sr=reshape(cell2mat(avr_cumulative_regret(:,method)),n_iterations,[]);
    mean_sr=mean(sr,2);
    mean_sr_samples=mean_sr(mark_index);
    plot(mark_index-1,mean_sr_samples,mark{method});
    hold on;
end
legend(leg{1:end});

for method=1:3
    sr=reshape(cell2mat(avr_cumulative_regret(:,method)),n_iterations,[]);
    mean_sr=mean(sr,2);
    plot(T,mean_sr,color{method});
    mean_sr_samples=mean_sr(mark_index);
    std_sr=std(sr,0,2);
    std_sr_samples=std_sr(mark_index);
    errorbar(mark_index-1,mean_sr_samples,std_sr_samples/4,mark{method});
end

xlabel('Iterations');
ylabel('R_T/T');
xlim([0 105]);
set(gca,'XTick',xtk);
saveas(gcf,[result_folder fun_name '_avr_cumulative_regret.eps'],'epsc2');
close all;
clear;

end


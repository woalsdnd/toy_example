function plotEachIteration(model,next_x)
%   plot the GP model for each iteration
%
%   INPUT : model, next point, plotIteration   
%

% set FLAGS (0 or 1)
plotMean=1;
plotReductionLCB=1;
plotLCB=1;

out_dir_name=[getenv('HOME_DIR') 'image/'];
if ~exist(out_dir_name, 'dir'), mkdir(out_dir_name);    end

% set grid
grid_1D=0:0.1:6;
[X,Y]=meshgrid(grid_1D,grid_1D);

% calculate posterior mean, variance, GP-LCB
[mean,var]=arrayfun(@(x,y) SEard_mean_var(model,[x,y]), X,Y);
sigma = sqrt(var);

% GP-LCB
% coeff is following the continuous case in the original GP-LCB paper
delta = 0.1;t=model.n;d=model.dim;r=2;a=1;b=1;
coeff = 2*log(t^2*2*pi^2/(3*delta))+2*d*log(t^2*d*b*r*sqrt(log(4*d*a/delta)));
lcb = mean-sqrt(coeff)*sigma;

if plotMean
    figure; 
    pcolor(X,Y,mean); shading flat;colorbar;caxis(model.plot_range);
    hold on;
    scatter(model.X(:,1),model.X(:,2),30,'k','filled'); 
    scatter(next_x(:,1),next_x(:,2),30,'w','filled'); 
    scatter(model.min_x(1),model.min_x(2),30,'r','filled'); 
    saveas(gcf,[out_dir_name num2str(model.n) '_posterior_mean.eps'],'epsc2');
    close all;

end

if plotReductionLCB
    figure;
    pcolor(X,Y,lcb-mean); shading flat;colorbar;%caxis([0,5]);
    hold on;
    scatter(model.X(:,1),model.X(:,2),30,'k','filled'); 
    scatter(next_x(:,1),next_x(:,2),30,'w','filled'); 
    scatter(model.min_x(1),model.min_x(2),30,'r','filled'); 
    saveas(gcf,[out_dir_name num2str(model.n) '_lcb_reduction.eps'],'epsc2');
    close all;
end

if plotLCB
    figure;
    pcolor(X,Y,lcb); shading flat;colorbar;caxis(model.plot_range);
    hold on;
    scatter(model.X(:,1),model.X(:,2),30,'k','filled'); 
    scatter(next_x(:,1),next_x(:,2),30,'w','filled'); 
    scatter(model.min_x(1),model.min_x(2),30,'r','filled'); 
    saveas(gcf,[out_dir_name num2str(model.n) '_lcb.eps'],'epsc2');
    close all;
end

end



function lml = log_likelihood(hyp_opt, model)
    %return Gaussian log likelihood
    %
    
    %set hyperparameters (hyp for optimization (length scales) & signal)
    hyp=[hyp_opt;model.hyp(end)];
      
    % Compute a kernel matrix 
    cov = (model.cov_model(hyp,model.X,model.X) + exp(2*model.noise)*eye(model.n));
    
    %calculate log likelihood (dropping constant)
    [Chol,p] = chol(cov,'lower');
    if p==0
        alpha = (Chol'\(Chol\model.f));
        lml = -model.f'*alpha - 2*sum(log(diag(Chol)));
    else
        warning('cholesky decomposition failed');
        lml=-inf;
    end
end


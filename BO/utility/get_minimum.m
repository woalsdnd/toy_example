function minimum=get_minimum(fun_name)
% return the minimum value of the problems

if strcmp(fun_name,'monetary_penalty')
    minimum=23.5549;
elseif strcmp(fun_name,'travel_time')
    minimum=917.5500;
end

end

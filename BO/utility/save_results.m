function save_results(fun_name,run,model)
% save simple regret & average cumulative regret
%

%set name of the output file
out_dir_name=[getenv('HOME_DIR') 'results/'];
if ~exist(out_dir_name, 'dir'), mkdir(out_dir_name);    end
out_file_name=[out_dir_name 'result_' fun_name '.mat'];

%load previously saved data
if exist(out_file_name, 'file') == 2,   load(out_file_name);    end

%retrieve optimum value of the function
minimum=get_minimum(fun_name);

%calculate simple regret and average cumulative regret
N=model.n-model.n_init_pts; T=(1:N);
simple_regret{run}=model.history_opt(2:end,model.dim+1)-minimum;
acr=model.f(model.n_init_pts+1:end,1);
regret=acr-minimum; avr_cumulative_regret{run}=zeros(N,1);
for i=1:N
    avr_cumulative_regret{run}(i)=sum(regret(1:i))/T(i);
end

%save results of the experiment
if exist(out_file_name, 'file') == 2
    save(out_file_name,'simple_regret','avr_cumulative_regret','-append');
else
    save(out_file_name,'simple_regret','avr_cumulative_regret');
end


end


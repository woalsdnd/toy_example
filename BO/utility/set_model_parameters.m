function model=set_model_parameters(model,hrange,n_init_pts,max_itr,bounds,hyp_update_cycle)
%   add necessary parameters based on user-specific, function-specific
%   parameters
%
%   INPUT : model (with user-specific, function-specific parameters), 
%           range of hyperparameters, #initial points, #max_iteration in BO loop, 
%           bounds in the domain, cycle of updating hyperparameters
%
%   OUTPUT : model with additional fields 
%

% set #current points, #initial points, #iterations in BO loop, 
%    cycle of updating hyperparameters, dimension
model.n = length(model.f);
model.n_init_pts=n_init_pts;
model.max_itr=max_itr;
model.hyp_update_cycle=hyp_update_cycle;
model.dim=2;

% set a kernel type corresponding to the method
model.kernel_type='ard';
model.cov_model = @(hyp, x, z)covSEard(hyp, x, z);

% set domain bounds
model.bounds=bounds;

% set boundary for hyperparameters in logarithm for optimization 
model.hyper_bound=log([ones(model.dim,1)*hrange.lengthscale;hrange.signal]);
model.noise_bound=log(hrange.noise);

% initialize hyperparameters (ell1,ell2,...,signal) in ard kernel and noise
% to the middle values in the ranges
model.hyp = log([ones(model.dim, 1)*hrange.lengthscale(1) ; mean(hrange.signal)]);      
model.noise = log(mean(hrange.noise));

% set lower cholesky
model.L=[];

% set history of optimal points and chosen points
model.history_opt=[];

end

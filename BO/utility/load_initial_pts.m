function [init_pts,init_fs]=load_initial_pts(fun_name,n_init_pts)
% INPUT : 
%         function name
%         number of initial points
%
% OUTPUT : initial points,
%          initial function values
%

indices=randperm(61*61);    indices=indices(1:n_init_pts);
tmp=load([getenv('HOME_DIR') 'simulation_results.mat']);
if strcmp(fun_name,'monetary_penalty')
    init_pts=tmp.init_pts(indices,:);
    init_fs=-tmp.score(indices);
elseif strcmp(fun_name,'travel_time')
    init_pts=tmp.init_pts(indices,:);
    init_fs=-tmp.travel_time(indices);
end
end

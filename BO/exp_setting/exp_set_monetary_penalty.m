function model=exp_set_monetary_penalty(fun_name,n_init_pts,max_itr,hyp_update_cycle)
% Initialize GP model specifying the followings : 
% #initial points, #points, input points, function values, 
% #maximum iterations (in the BO loop), 
% (box constrained) domain, 
% hyperparamters, range of hyperparameters, 
% prior mean,
% function name,
% lower cholesky matrix, 
% history (in optimization), current min, current min point.
% cycle of updating hyperparameters
%

%% function-specific parameters
sec=datevec(now); seed = 1000*sec(6); 
randn('seed',seed), rand('seed',seed);

% set prior mean
model.prior_mean=26.7;

% set output range for plot
model.plot_range=[23,33];

% set a range of hyperparameters (lengthscale, sigma_f, sigma_n)
hrange.lengthscale=[0.5,2];
hrange.signal=[1,1]; %fix signal and noise (rule of thumb)
hrange.noise=[0.1,0.1];

% set constraint on the domain ([0,6]^2) and objective function
bounds_domain=[0,6;0,6];

%% set objection function, initial points and corresponding function values
model.obj=str2func(fun_name);
[model.X,model.f]=load_initial_pts(fun_name,n_init_pts);

%% set model parameters
model=set_model_parameters(model,hrange,n_init_pts,max_itr,bounds_domain,hyp_update_cycle);

%% update Cholesky matrix, optimal value, optimizer, history
model=updateGPmodel(model);

end

%% set a function name and interval of iterations for plot (negative value means no plots)
fun_name='monetary_penalty';
% fun_name='travel_time';
plotIteration=-1;

%% set parameters
n_runs=20;
max_itr=100;
n_init_pts=1;
hyp_update_cycle=30;
        
%% run BO (minimization problems)
for run=1:n_runs
    % initialize a model with the experiment settings 
    fh=str2func(['exp_set_' fun_name]);
    model=fh(fun_name,n_init_pts,max_itr,hyp_update_cycle);

    % run Bayesian Optimization
    model=BayesianOptimization(model,plotIteration);

    % save the results
    save_results(fun_name,run,model);
end

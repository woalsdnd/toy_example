function acq = SEard_acq(model, x)
%% return acquisition function (GP-LCB)
%

% prevent duplicates by setting to an infinite value
if sum(ismember(model.X,x,'rows'))>0
    acq=realmax;
else
    [mu, var] = SEard_mean_var(model, x);
    sigma = sqrt(var);

    % GP-LCB
    % coeff is following the continuous case in the original GP-LCB paper
    delta = 0.1;t=model.n;d=model.dim;r=2;a=1;b=1;
    coeff = 2*log(t^2*2*pi^2/(3*delta))+2*d*log(t^2*d*b*r*sqrt(log(4*d*a/delta)));
    acq = mu-sqrt(coeff)*sigma;
end
end

function next_point=retrieve_next(model)

% set grid
grid_1D=0:0.1:6;
[X,Y]=meshgrid(grid_1D,grid_1D);

% evaluate LCB
LCB=arrayfun(@(x,y) SEard_acq(model, [x y]),X,Y);

% find the minimizer
[min_val,ind]=min(LCB(:));
next_point=[X(ind),Y(ind)];



end

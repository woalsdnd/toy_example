function model=BayesianOptimization(model,plotIteration)
% INPUT : initialized BO model, flag for plotting each iteration 
%
% OUTPUT : trained model
%

for i=1:model.max_itr
    % retrieve the next point
    next_x=retrieve_next(model);
    f=model.obj(next_x);
    
    if plotIteration>0 && mod(i,plotIteration)==0
        % plot current GP model, anova_dcop (debug), loglikelihood
        plotLogLikelihood(model);
        plotEachIteration(model,next_x);
    end
    % augment new data and update kernel matrix, optimal value, optimizer
    model.X=[model.X;next_x];   model.f=[model.f;f];    model.n=model.n+1;
    model=updateGPmodel(model); 

    fprintf('current minimum : %d\n',model.min_val);
end
end

function updated_model=updateGPmodel(model)
% update lower cholesky matrix, minimum value, minimum point, history   
%

% update min value and minimizer
[model.min_val, argmin]= min(model.f);
model.min_x=model.X(argmin,:);
model.history_opt=[model.history_opt;[model.min_x model.min_val]];

% update hypers periodically
if mod(model.n,model.hyp_update_cycle)==0
    % learn hyper parameters 
    tic;
    model.hyp=learn_hyperparameters(model);
    fprintf('Learning Hypers : %d sec\n',toc);
end

% update lower cholesky and kernel matrices if necessary
if mod(model.n,model.hyp_update_cycle)==0 || size(model.L,1)==0
    cov = model.cov_model(model.hyp,model.X,model.X) + exp(2*model.noise)*eye(model.n);
    model.L = chol(cov, 'lower');
else
    % append new row on the previous cholesky matrix
    prev_X = model.X(1:model.n-1,:);
    new_x=model.X(model.n,:);
    cross_cov = model.cov_model(model.hyp,prev_X,new_x);
    auto_cov = model.cov_model(model.hyp,new_x,new_x)+exp(2*model.noise);
    new_column = model.L\cross_cov;
    new_corner = sqrt(auto_cov - new_column'*new_column);
    model.L = [[model.L, zeros(model.n-1, 1)];[new_column', new_corner]];
end

updated_model=model;

end

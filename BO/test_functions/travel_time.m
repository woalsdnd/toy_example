function y=travel_time(x)

if length(x)~=2
    error('function takes 2 dimensional inputs');
end

tmp=load('simulation_results.mat');
queried=3721-610*x(2)-10*x(1);
index=round(3721-610*x(2)-10*x(1));

if abs(queried-index)>0.1
    error('queried value should be an integer');
end

y=-tmp.travel_time(index,1);

end
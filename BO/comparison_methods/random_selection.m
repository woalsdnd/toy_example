%set necessary settings
n_runs=20;
tmp=load([getenv('HOME_DIR') 'simulation_results.mat']);
fun_name='monetary_penalty';
% fun_name='travel_time';
if strcmp(fun_name,'monetary_penalty')
    out_file_name=[getenv('HOME_DIR') 'results/random_monetary_penalty.mat'];
elseif strcmp(fun_name,'travel_time')
    out_file_name=[getenv('HOME_DIR') 'results/random_travel_time.mat'];
end

for i=1:n_runs
    indices=randperm(61*61);    indices=indices(1:100);
    if strcmp(fun_name,'monetary_penalty')
        monetary_penalty=-tmp.score(indices);
        %save results of the experiment
        if exist(out_file_name, 'file') == 2
            a=load(out_file_name);
            monetary_penalty=[a.monetary_penalty;monetary_penalty];
            save(out_file_name,'monetary_penalty','-append');
        else
            save(out_file_name,'monetary_penalty');
        end
    elseif strcmp(fun_name,'travel_time')
        travel_time=-tmp.travel_time(indices);
        %save results of the experiment
        if exist(out_file_name, 'file') == 2
            a=load(out_file_name);
            travel_time=[a.travel_time;travel_time];
            save(out_file_name,'travel_time','-append');
        else
            save(out_file_name,'travel_time');
        end
    end
 
end
%set function handler and number of variables    
fun=@monetary_penalty;
nvars=2;
n_runs=20;

%set options
lb=[0,0];
ub=[6,6];
options=gaoptimset('Generations',8,'PopulationSize',10,'OutputFcn',@addhistory_ga_monetary_penalty);

%run genetic algorithm
for i=1:n_runs
    [x,fval,exitFlag,output,population,scores] = ga(fun,nvars,[],[],[],[],lb,ub,[],options);
end
function [state,options,optchanged ] = addhistory_ga_travel_time(options,state,flag)
    optchanged = false;
    
    %set name of the output file
    out_dir_name=[getenv('HOME_DIR') 'results/'];
    if ~exist(out_dir_name, 'dir'), mkdir(out_dir_name);    end
    out_file_name=[out_dir_name 'ga_travel_time.mat'];
    
    travel_time=state.Score;
    
    %save results of the experiment
    if exist(out_file_name, 'file') == 2
        a=load(out_file_name);
        travel_time=[a.travel_time;travel_time];
        save(out_file_name,'travel_time','-append');
    else
        save(out_file_name,'travel_time');
    end
end

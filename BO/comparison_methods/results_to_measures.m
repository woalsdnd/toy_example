function results_to_measures(file_name,output_fn)
% load data from the file
result_dir=[getenv('HOME_DIR') 'results/'];
fn=[result_dir file_name];
tmp=load(fn);

% read the function values
if isfield(tmp,'travel_time')
    fs=reshape(tmp.travel_time,100,20);
    minimum=get_minimum('travel_time');
elseif isfield(tmp,'monetary_penalty')
    fs=reshape(tmp.monetary_penalty,100,20);
    minimum=get_minimum('monetary_penalty');
end

% compute simple regret and average cumulative regret
N=size(fs,1);   T=(1:N);
for run=1:20
    regret=fs(:,run)-minimum;
    for i=1:N
        simple_regret{run}(i,1)=min(regret(1:i));
        avr_cumulative_regret{run}(i,1)=sum(regret(1:i))/T(i);  
    end
end

save([result_dir output_fn],'simple_regret','avr_cumulative_regret');

end
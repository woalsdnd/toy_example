function [state,options,optchanged ] = addhistory_ga_monetary_penalty(options,state,flag)
    optchanged = false;
    
    %set name of the output file
    out_dir_name=[getenv('HOME_DIR') 'results/'];
    if ~exist(out_dir_name, 'dir'), mkdir(out_dir_name);    end
    out_file_name=[out_dir_name 'ga_monetary_penalty.mat'];
    
    monetary_penalty=state.Score;
    
    %save results of the experiment
    if exist(out_file_name, 'file') == 2
        a=load(out_file_name);
        monetary_penalty=[a.monetary_penalty;monetary_penalty];
        save(out_file_name,'monetary_penalty','-append');
    else
        save(out_file_name,'monetary_penalty');
    end
end

import subprocess
import time
import os
import shutil
import sys
import xml.etree.cElementTree as ET
import numpy as np
import scipy.io as sio
from sys import argv

def collectFvalues(project_folder):
    
    config_folder = project_folder + "config/"
    template_folder = config_folder + "template/"
    simulation_output_folder = project_folder + "simulation_results/"
    
    # create plans.xml, simple_network.xml, config_template.xml under the config folder (path changed corresponding to the project folder)
    with open(os.devnull, "w") as f:
        subprocess.call("sed \"s|plans_dtd|%splans_v4.dtd|g\" <%splans_template.xml >%splans.xml" % (config_folder, template_folder, config_folder), shell=True, stdout=f, stderr=f)
    with open(os.devnull, "w") as f:
        subprocess.call("sed \"s|network_dtd|%snetwork_v1.dtd|g\" <%ssimple_network_template.xml >%ssimple_network.xml" % (config_folder, template_folder, config_folder), shell=True, stdout=f, stderr=f)
    with open(os.devnull, "w") as f:
        subprocess.call("sed \"s|config_dtd|%sconfig_v1.dtd|g;s|path_to_outputfolder|%smatsim_output|g;s|path_to_plans_file|%splans.xml|g;s|path_to_network_file|%ssimple_network.xml|g;s|path_to_toll_file|%sTOLL/toll.xml|g\" <%sconfig_template.xml >%sconfig_template.xml" 
                        % (config_folder, simulation_output_folder, config_folder, config_folder, config_folder, template_folder, config_folder), shell=True, stdout=f, stderr=f)
    
    # load toll links
    toll_links = [line.rstrip('\n') for line in open(config_folder + "toll_links")]
    
    # specify the price according to var_file
    grid_d1 = np.linspace(0, 6, 61, endpoint=True)
    tp1, tp2 = np.meshgrid(grid_d1, grid_d1)

    for xi in range(61):
        for yi in range(61):
            # set a file identifier (p2,p5)
            serial_num = str(yi).zfill(2) + "_" + str(xi).zfill(2)

            # create xml structure
            root = ET.Element("roadpricing", type="cordon", name="singapore erp")
            links = ET.SubElement(root, "links")
            
            # set toll prices
            tolls = [tp1[xi][yi], tp2[xi][yi]]
            i = 0
            for link in toll_links:
                link_tuple = ET.SubElement(links, "link", id=str(link))
                ET.SubElement(link_tuple, "cost", start_time="07:00", end_time="12:00", amount=str(tolls[i]))  # only care about morning rush hours 
                i = i + 1
                
            # output into xml    
            xml = ET.ElementTree(root)
            with open(config_folder + "TOLL/toll_%s.xml" % (serial_num), 'w') as f:
                f.write('<?xml version="1.0" encoding="UTF-8" ?><!DOCTYPE roadpricing SYSTEM "' + config_folder + 'roadpricing_v1.dtd">')
                xml.write(f, 'utf-8')
            
            # create config_serial_num.xml changing output folder name
            with open(os.devnull, "w") as f:
                subprocess.call("sed \"s/matsim_output/toll_prices_%s/g;s/toll.xml/toll_%s.xml/g\" <%sconfig_template.xml >%sCONFIG/config_%s.xml" % (serial_num, serial_num, config_folder, config_folder, serial_num), shell=True, stdout=f, stderr=f)
            
            # run MATSIM
            cp = "%s:%slibs/*" % (project_folder, project_folder)
            config = "%sconfig/CONFIG/config_%s.xml" % (project_folder, serial_num)
            subprocess.call("java -Xmx1g -cp %s org.matsim.run.Controler %s" % (cp, config),
            shell=True, stdout=open(config_folder + "LOG/log_%s" % (serial_num) , "w"), stderr=open(config_folder + "LOG/err_log_%s" % (serial_num), "w"))
             
            # read scores from the file (max of last two scores to cover the case of decrement)
            lines = [line.rstrip('\n') for line in open(simulation_output_folder + "toll_prices_%s/ITERS/it.15/15.tripdurations.txt" % (serial_num)) ]
            travel_time = float(lines[len(lines) - 1].split(" ")[3])
            lines = [line.rstrip('\n') for line in open(simulation_output_folder + "toll_prices_%s/scorestats.txt" % (serial_num)) ]
            score = float(lines[len(lines) - 1].split("\t")[1])
            print(str(tp1[xi][yi]) + " , " + str(tp2[xi][yi]) + " : " + str(-score) + " (penalty), " + str(travel_time) + " (travel time)")
            
            # save results
            if os.path.exists("%s/result.mat" % project_folder):
                data = sio.loadmat("%s/result.mat" % project_folder)
                tolls = np.append(np.asmatrix(tolls), data['init_pts'], axis=0)
                travel_time = np.append(np.asmatrix(travel_time), -data['travel_time'], axis=0)
                score = np.append(np.asmatrix(score), data['score'], axis=0)
            sio.savemat("%s/result.mat" % project_folder, {'init_pts':tolls, 'score':score, 'travel_time':-travel_time})
            
            # save original results 
            with open(project_folder + "OUTS/input_%s.txt" % (serial_num), 'w') as f:
                f.write(str(tolls))
            shutil.copy(simulation_output_folder + "toll_prices_%s/ITERS/it.15/15.tripdurations.txt" % (serial_num), project_folder + "OUTS/tripdurations_%s.txt" % (serial_num))
            shutil.copy(simulation_output_folder + "toll_prices_%s/scorestats.txt" % (serial_num), project_folder + "OUTS/scorestats_%s.txt" % (serial_num))
            
if __name__ == '__main__':
    collectFvalues(sys.argv[1])

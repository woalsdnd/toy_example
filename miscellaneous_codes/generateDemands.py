import sys
import time
import xml.etree.cElementTree as ET

def generate_demands_braess(out_fn):
    plans=ET.Element("plans")

    for i in range(1,101):
        #generate xml tags (person id starts with 1)
        person=ET.SubElement(plans,"person",id=str(i))
        plan=ET.SubElement(person,"plan")
        ET.SubElement(plan,"act",type="h",x="-1",y="0", end_time=time.strftime("%H:%M:%S", time.gmtime(8*3600+i*30))) 
        ET.SubElement(plan,"leg",mode="car")
        ET.SubElement(plan,"act",type="w",x="3465",y="2000",start_time=time.strftime("%H:%M:%S", time.gmtime(9*3600+i*30)))
        xml=ET.ElementTree(plans)
    
    with open(out_fn, 'w') as f:
        f.write('<?xml version="1.0" encoding="UTF-8" ?><!DOCTYPE plans SYSTEM "plans_dtd">')
        xml.write(f, 'utf-8')

if __name__ == "__main__":
    generate_demands_braess(sys.argv[1])

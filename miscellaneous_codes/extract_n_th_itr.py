import sys
import numpy as np
import os
import scipy.io as sio

def extract_n_th_itr(simulation_output_folder, n, output_file):

    # specify the price according to var_file
    grid_d1 = np.linspace(0, 6, 61, endpoint=True)
    tp1, tp2 = np.meshgrid(grid_d1, grid_d1)

    for xi in range(61):
        for yi in range(61):
            target_folder = simulation_output_folder + "toll_prices_" + str(yi).zfill(2) + "_" + str(xi).zfill(2) + "/"
            tolls = [tp1[xi][yi], tp2[xi][yi]]
            # read scores from the file (max of last two scores to cover the case of decrement)
            lines = [line.rstrip('\n') for line in open(target_folder + "ITERS/it." + n + "/" + n + ".tripdurations.txt") ]
            travel_time = float(lines[len(lines) - 1].split(" ")[3])
            lines = [line.rstrip('\n') for line in open(target_folder + "scorestats.txt") ]
            score = float(lines[int(n)].split("\t")[1])
            print(str(tp1[xi][yi]) + " , " + str(tp2[xi][yi]) + " : " + str(-score) + " (penalty), " + str(travel_time) + " (travel time)")

            # save results
            if os.path.exists(output_file + ".mat"):
                data = sio.loadmat(output_file + ".mat")
                tolls = np.append(np.asmatrix(tolls), data['init_pts'], axis=0)
                travel_time = np.append(np.asmatrix(travel_time), -data['travel_time'], axis=0)
                score = np.append(np.asmatrix(score), data['score'], axis=0)
            sio.savemat(output_file + ".mat", {'init_pts':tolls, 'score':score, 'travel_time':-travel_time})
       
    
if __name__ == "__main__":
    extract_n_th_itr(sys.argv[1], sys.argv[2],sys.argv[3])

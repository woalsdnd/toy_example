function []= draw_score()

score=load('../MATSim_RoadPricing/simulation_results/scorestats.txt');

%plot score
figure;
plot(1:length(score(:,2)),-score(:,2));
xlim([-10 length(score(:,2))+10]);


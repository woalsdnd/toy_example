function plotFnc2D(result_file)
% load the data
load(result_file);

% prepare the function
grid_1D=0:0.1:6;
n_grid_1D=length(grid_1D);
[X,Y]=meshgrid(grid_1D,grid_1D);
f_penalty=zeros(n_grid_1D,n_grid_1D);
f_travel_time=zeros(n_grid_1D,n_grid_1D);
for i=60:-1:0
    for j=61:-1:1
        f_penalty(61-i,62-j)=-score(61*i+j,1);
        f_travel_time(61-i,62-j)=-travel_time(61*i+j,1);
    end
end

% plot in 2D
figure;
pcolor(X,Y,f_penalty);shading flat;h=colorbar;ylabel(h, 'SGD');
xlabel('p2');  ylabel('p5');
saveas(gcf,'./penalty.eps','epsc2');
close all;
figure;
pcolor(X,Y,f_travel_time);shading flat;h=colorbar;ylabel(h, 'second');
xlabel('p2');  ylabel('p5');
saveas(gcf,'./travel_time.eps','epsc2');
close all;

% plot in 3D
% figure;
% surf(X,Y,f_penalty);   shading flat;   h=colorbar;    ylabel(h, 'SGD');
% xlabel('p2');  ylabel('p5');
% saveas(gcf,'./penalty_3d.eps','epsc2');
% close all;
% surf(X,Y,f_travel_time);   shading flat;   h=colorbar;    ylabel(h, 'second');
% xlabel('p2');  ylabel('p5');
% saveas(gcf,'./travel_time_3d.eps','epsc2');
% close all;

end